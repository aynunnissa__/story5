from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class MataKuliah(models.Model) :
	nama = models.CharField(max_length=45)
	dosen = models.CharField(max_length=200)
	sks = models.IntegerField(default=1, validators=[MinValueValidator(0)])
	deskripsi = models.TextField()
	semester = models.CharField(
		max_length=5,
		verbose_name='Semester',
		choices=[
	        ('Genap', 'Genap'),
	        ('Gasal', 'Gasal'),
	    ],
    )
	tahunAjaran1 = models.IntegerField(default=0,validators=[MinValueValidator(0), MaxValueValidator(9999)])
	tahunAjaran2 = models.IntegerField(default=0,validators=[MinValueValidator(0), MaxValueValidator(9999)])
	ruangKelas = models.CharField(max_length=200)

	def __str__(self):
		return self.nama