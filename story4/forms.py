from django import forms
from django.contrib.auth.models import User
from .models import MataKuliah
from django.db import models

class MatkulForm(forms.ModelForm):

	class Meta:
		model = MataKuliah
		fields = ['nama','sks', 'dosen','ruangKelas','semester', 'tahunAjaran1','tahunAjaran2','deskripsi']

