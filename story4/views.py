from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.generic import TemplateView
from .forms import MatkulForm
from .models import MataKuliah

def homepage(request) :
	return render(request, 'story4/index.html', {'homepage' : 1})

def project(request) :
	return render(request, 'story4/project.html', {'project' : 1})

def interest(request) :
	return render(request, 'story4/interests.html', {'interest' : 1})

def matkul(request) :
	daftarMatkul = MataKuliah.objects.all().order_by('-tahunAjaran1','-semester','nama')
	return render(request, 'story4/matkul.html', {'matkul' : 1, 'daftarMatkul' : daftarMatkul})

def detailMatkul(request, id) :
	matkul = get_object_or_404(MataKuliah, id=id)
	return render(request, 'story4/detail_matkul.html', {'matkul' : matkul})

def addMatkul(request) :
	daftarMatkul = MataKuliah.objects.all().order_by('-tahunAjaran1','-semester','nama')
	if request.method == 'POST' :
		form = MatkulForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('matkul')
	else :
		form = MatkulForm()
	return render(request,'story4/add_matkul.html',{'form':form,'daftarMatkul' : daftarMatkul})

def deleteMatkul(request, id):
	matkul = get_object_or_404(MataKuliah, id=id)
	matkul.delete()
	return redirect('addMatkul')