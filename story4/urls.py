from django.urls import path

from . import views
# from story4.views import Homepage, Project, Interest

urlpatterns = [
	path('', views.homepage, name='index'),
    path('project/', views.project, name='project'),
    path('interests/', views.interest, name='interest'),
    path('matkul/', views.matkul, name='matkul'),
    path('addMatkul/', views.addMatkul, name='addMatkul'),
    path('detailMatkul/<int:id>', views.detailMatkul, name="detailMatkul"),
    path('deleteMatkul/<int:id>', views.deleteMatkul, name="deleteMatkul"),
]